CREATE DATABASE ejercicioCosmeticosMapeo;
USE ejercicioCosmeticosMapeo;

CREATE TABLE cliente
(
    idCliente  int AUTO_INCREMENT PRIMARY KEY,
    Nombre     varchar(50)       NOT NULL,
    Dni        varchar(9) UNIQUE NOT NULL,
    CP         int               NOT NULL,
    Direccion  varchar(100)      NOT NULL,
    Ciudad     varchar(40)       NOT NULL,
    NumeroTlfn int               NOT NULL

);


CREATE TABLE productos
(
    idProducto     int AUTO_INCREMENT PRIMARY KEY,
    Nombre         varchar(40) NOT NULL UNIQUE,
    Precio         float       NOT NULL,
    Modelo         varchar(50) NOT NULL,
    Marca          varchar(50) NOT NULL,
    PH             float       NOT NULL,
    FechaCaducidad DATE   NOT NULL,
    PesoGramo float NOT NULL
    

);
CREATE TABLE tienda(
    idTienda  int AUTO_INCREMENT PRIMARY KEY,
    Direccion varchar(40) NOT NULL,
    Poblacion int         NOT NULL,
    Comunidad varchar(40) NOT NULL,
    Provincia varchar(40) NOT NULL,
    CP        int         NOT NULL,
    Tlfn      int         NOT NULL
);

CREATE TABLE paginaWeb(
idPagina int AUTO_INCREMENT PRIMARY KEY,
idTienda int NOT NULL,
Nombre varchar(40) NOT NULL,
Dominio varchar(5) NOT NULL,
Url varchar(100) NOT NULL,
FOREIGN KEY (idTienda) REFERENCES tienda (idTienda)
);


CREATE TABLE trabajador(
	idTrabajador     int AUTO_INCREMENT PRIMARY KEY,
    idTienda int NOT NULL,
	Nombre varchar(40) NOT NULL,
    Apellidos varchar(40) NOT NULL,
    Dni varchar(9) NOT NULL UNIQUE,
    Correo varchar(40) NOT NULL,
    Edad varchar(3) NOT NULL,
	FOREIGN KEY (idTienda) REFERENCES tienda (idTienda)
);





CREATE TABLE clienteProducto
(
    idCliente         int NOT NULL,
    idProducto        int NOT NULL,
    FOREIGN KEY (idCliente) REFERENCES cliente (idCliente),
    FOREIGN KEY (idProducto) REFERENCES productos (idProducto)
);

CREATE TABLE productosTienda(
    idTienda         int NOT NULL,
    idProducto       int NOT NULL,
    cantidad		 int NOT NULL,
	FOREIGN KEY (idProducto) REFERENCES productos (idProducto),
    FOREIGN KEY (idTienda) REFERENCES tienda (idTienda)
);

